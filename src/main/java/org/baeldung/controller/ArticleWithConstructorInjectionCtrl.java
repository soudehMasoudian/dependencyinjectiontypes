package org.baeldung.controller;

import org.baeldung.persistence.model.BaeldungArticle;
import org.baeldung.persistence.service.BaeldungArticleManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by Masoudian on 1/28/2018.
 */
@RestController
public class ArticleWithConstructorInjectionCtrl {
    private BaeldungArticleManager constructorBasedManager;

    //constructor-based
    @Autowired
    public ArticleWithConstructorInjectionCtrl(BaeldungArticleManager manager) {
        this.constructorBasedManager = manager;
    }

    @RequestMapping(method = RequestMethod.GET, value = "/article/constructor/{id}")
    public BaeldungArticle fetchArticle(@PathVariable long id, Model model) {
        BaeldungArticle  article =constructorBasedManager.fetchArticle(id);
        model.addAttribute("article", article);
        return article;
    }
}
