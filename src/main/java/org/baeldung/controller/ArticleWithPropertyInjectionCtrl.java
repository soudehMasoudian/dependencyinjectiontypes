package org.baeldung.controller;

import org.baeldung.persistence.model.BaeldungArticle;
import org.baeldung.persistence.service.BaeldungArticleManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

@RestController
public class ArticleWithPropertyInjectionCtrl {

    // property
    @Autowired
    BaeldungArticleManager baeldungArticleManager;

    @RequestMapping(method = RequestMethod.GET, value = "/article/property/{id}")
    public BaeldungArticle fetchArticle(@PathVariable long id, Model model) {
        BaeldungArticle  article =baeldungArticleManager.fetchArticle(id);
        model.addAttribute("article", article);
        return article;
    }
}
