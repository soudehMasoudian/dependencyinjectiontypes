package org.baeldung.persistence.service.impl;

import org.baeldung.persistence.dao.BaeldungArticleRepository;
import org.baeldung.persistence.model.BaeldungArticle;
import org.baeldung.persistence.service.BaeldungArticleManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by Masoudian on 1/28/2018.
 */
@Service
public class BaeldungArticleManagerImpl implements BaeldungArticleManager {

    @Autowired
    BaeldungArticleRepository baeldungArticleRepository;

    public BaeldungArticleManagerImpl() {
    }

    public BaeldungArticleManagerImpl(String s) {
    }

    @Override
    public BaeldungArticle fetchArticle(long id) {
        BaeldungArticle article = baeldungArticleRepository.fetchArticle(id);
        return article;
    }
}
