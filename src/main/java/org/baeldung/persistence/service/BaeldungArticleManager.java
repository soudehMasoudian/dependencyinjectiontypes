package org.baeldung.persistence.service;

import org.baeldung.persistence.model.BaeldungArticle;

/**
 * Created by Masoudian on 1/28/2018.
 */
public interface BaeldungArticleManager {
    BaeldungArticle fetchArticle(long id);
}
