package org.baeldung.persistence.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Date;

/**
 * Created by Masoudian on 1/28/2018.
 */
@Entity
@Table(name = "t_baeldung_article")
public class BaeldungArticle implements Serializable {

    private static final long serialVersionUID = 7203164772490409578L;

    @Id
    @Column
    private Long id;

    @Column
    private String title;

    @Column
    private String authorName;

    @Column
    private Date entryDate;

    public BaeldungArticle() {
    }

    public BaeldungArticle(String title) {
    }

    public Long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAuthorName() {
        return authorName;
    }

    public void setAuthorName(String authorName) {
        this.authorName = authorName;
    }

    public Date getEntryDate() {
        return entryDate;
    }

    public void setEntryDate(Date entryDate) {
        this.entryDate = entryDate;
    }
}
