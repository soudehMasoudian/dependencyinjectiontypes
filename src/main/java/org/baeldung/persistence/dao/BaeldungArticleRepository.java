package org.baeldung.persistence.dao;

import org.baeldung.persistence.model.BaeldungArticle;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by Masoudian on 1/28/2018.
 */
@Repository
public interface BaeldungArticleRepository extends JpaRepository<BaeldungArticle, Long>
{
    @org.springframework.data.jpa.repository.Query("select ba from BaeldungArticle ba where ba.id = ?1" )
    BaeldungArticle fetchArticle(long id);
}
